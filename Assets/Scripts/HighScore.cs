﻿using UnityEngine;
using UnityEngine.UI;

public class HighScore : MonoBehaviour {

	public Text highScoreText;

	void Update () {
		highScoreText.text = "Highscore: " + FindObjectOfType<GameManager> ()._highScore;	
	}
}
