﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOptions : MonoBehaviour {

	public void RestartLevel(){
		SceneManager.LoadScene (1);
	}

	public void StartGame(){
		SceneManager.LoadScene (1);
	}

	public void ReturnMain(){
		SceneManager.LoadScene (0);
	}

	public void Quit(){
		Application.Quit ();
	}
}
