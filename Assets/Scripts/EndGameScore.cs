﻿using UnityEngine;
using UnityEngine.UI;

public class EndGameScore : MonoBehaviour {

	public Text endScore;

	void Update () {
		if (FindObjectOfType<GameManager>()._isHighScore == true) {
			endScore.text = "NEW HIGHSCORE!!! : " + FindObjectOfType<GameManager> ()._highScore;
		} else {
			endScore.text = "SCORE : " + FindObjectOfType<GameManager> ()._playerScore;
		}
	}
}
