﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerScore : MonoBehaviour {

	public Text scoreText;

	void Update () {
		scoreText.text = "Score: " + FindObjectOfType<GameManager> ()._playerScore;
	}
}
