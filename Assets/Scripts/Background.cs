﻿using UnityEngine;

public class Background : MonoBehaviour {

	public GameObject background1;
	public GameObject background2;
	private Rigidbody2D bgRigid1;
	private Rigidbody2D bgRigid2;

	void Start(){

		bgRigid1 = background1.GetComponent<Rigidbody2D> ();
		bgRigid2 = background2.GetComponent<Rigidbody2D> ();

		bgRigid1.gravityScale = -1;
		bgRigid2.gravityScale = -1;
	
	}

	void Update(){

		if(background1.transform.position.y >= 25){
			bgRigid1.velocity = Vector2.zero;
			background1.transform.position = new Vector2 (0, -25);
		}

		if(background2.transform.position.y >= 25){
			bgRigid2.velocity = Vector2.zero;
			background2.transform.position = new Vector2 (0, -25);
		}
	}

}
