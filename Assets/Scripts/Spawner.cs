﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

	public Transform[] spawner;
	public GameObject obstacles;
	public GameObject coins;

	private float SpawnTime = 5f;

	void Update(){

		if(FindObjectOfType<GameManager>()._gameOver == false){
			if(Time.time > SpawnTime){
				Spawn ();
				SpawnTime = Time.time + 2f;
			}
		}

	}

	void Spawn(){

		int numberOne = Random.Range (0, spawner.Length);
		int numberTwo = Random.Range (1, 4);

		if (FindObjectOfType<GameManager> ()._playerScore <= 30) {
			for (int count = 0; count < spawner.Length; count++) {

				if (numberOne != numberTwo) {

					if (numberOne != count && numberTwo != count) {
						Instantiate (obstacles, spawner [count].position, Quaternion.identity);
					} else {
						Instantiate (coins, spawner [count].position, Quaternion.identity);
					}

				} else {
					Instantiate (coins, spawner [count].position, Quaternion.identity);
				}

			}	
		} else {
			for (int count = 0; count < spawner.Length; count++) {

				if (numberOne != count) {
					Instantiate (obstacles, spawner [count].position, Quaternion.identity);
				} else {
					Instantiate (coins, spawner [count].position, Quaternion.identity);
				}

			}
		}




	}
}
