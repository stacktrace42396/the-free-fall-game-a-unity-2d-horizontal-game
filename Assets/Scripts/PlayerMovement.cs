﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour {
	
	public Rigidbody2D player;
	public float gameMapWidth = 3f;

	void FixedUpdate(){

		//	xPos to determine left or right
		//	negative means left
		//	positive means right
		float xPos = Input.GetAxis ("Horizontal") * 20f * Time.fixedDeltaTime;

		//	apply new position to a new Vector2
		//	will adjust the x position of sprite
		Vector2 newPlayerPos = player.position + (Vector2.right * xPos);

		//	limit the minimum and maximum movement of sprite
		newPlayerPos.x = Mathf.Clamp(newPlayerPos.x, -gameMapWidth, gameMapWidth);

		//	move the sprite in new position
		player.MovePosition (newPlayerPos);

	}

	void OnCollisionEnter2D(Collision2D _col){

		if (_col.collider.tag == "obstacle") {
			FindObjectOfType<GameManager> ().GameOver ();
		}
			
		if(_col.collider.tag == "coin") {
			Destroy (_col.gameObject);
			FindObjectOfType<GameManager> ().GameScore ();
		}
	}

}
