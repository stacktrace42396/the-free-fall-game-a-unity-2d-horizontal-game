﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

	private int playerScore = 0;
	private float slowness = 8f;
	private int hitCount = 0;
	private bool gameOver = false;
	private bool isHighScore = false;

	public GameObject gameOverUI;
	public AudioSource bonus;
	public AudioSource explosion;
	public AudioSource bgMusic;

	public int _playerScore{
		get{ return playerScore; }
		set{ playerScore = value; }
	}

	public bool _isHighScore{
		get{ return isHighScore; }
	}

	public bool _gameOver{
		get{ return gameOver; }
	}

	public int _highScore{
		get{ return PlayerPrefs.GetInt ("highscore"); }
	}

	public Transform player;

	void Awake(){
		float randomNumber = Random.Range (3, 7) / 10f;
		bonus.volume = randomNumber;
		explosion.volume = randomNumber;
		bgMusic.volume = randomNumber;
	}

	public void GameOver(){

		gameOver = true;
		hitCount++;

		if(gameOver == true && hitCount <= 1){
			explosion.Play ();
			if(playerScore > PlayerPrefs.GetInt("highscore")){
				PlayerPrefs.SetInt ("highscore", playerScore);
				StartCoroutine (SlowMotion());
				isHighScore = true;
			}else {
				StartCoroutine (SlowMotion());
			}
		}
			
	}

	public void GameScore(){
		if(gameOver != true){
			bonus.Play ();
			playerScore++;
		}
	}

	IEnumerator SlowMotion(){

		Time.timeScale = 1f / slowness;
		Time.fixedDeltaTime = Time.fixedDeltaTime / slowness;

		Camera.main.orthographicSize = 2f;
		Camera.main.transform.position = new Vector3 (player.position.x, player.position.y, -10);

		yield return new WaitForSeconds (1f / slowness);

		Time.timeScale = 1f;
		Time.fixedDeltaTime = 0.02f;

		gameOverUI.SetActive (true);
	}

}
